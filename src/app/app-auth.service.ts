import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable()
export class AppAuthService {
  url: any;

  constructor(private router: Router) {
    this.url = 'http://192.168.0.207:43210';
  }


  isLoggedIn() {
    return !!localStorage.getItem('token');
  }

}
