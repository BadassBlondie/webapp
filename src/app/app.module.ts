import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PostComponent } from './post/post.component';
import { ProfileComponent } from './profile/profile.component';
import { PublisherComponent } from './publisher/publisher.component';
import { UserinfoComponent } from './userinfo/userinfo.component';

import {AppAuthService} from "./app-auth.service";
import {AuthGuard} from "./auth.guard";

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    NavbarComponent,
    PostComponent,
    ProfileComponent,
    PublisherComponent,
    UserinfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [AppAuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
